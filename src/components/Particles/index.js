import Particles from "react-tsparticles"
import { loadFull } from "tsparticles"

function Particle() {

    const particlesInit = async (main) => {
        console.log(main)

        await loadFull(main);
    };

    const particlesLoaded = (container) => {
        console.log(container);
    }
    return (
    <div className="particol">
        <Particles
            id="tsparticles"
                init={particlesInit}
                Loaded={particlesLoaded}

                options={{
                    background: {
                        // color: {
                        //     value: "#011110",
                        // },
                    },
                    fpsLimit: 120,
                    interactivity: {
                        events: {
                            onClick: {
                                enable: false,
                                mode: "push",
                            },
                            onHover: {
                                enable: true,
                                mode: "repulse",
                            },
                            resize: true,
                        },
                        modes: {
                            push: {
                                quantity: 4,
                            },
                            repulse: {
                                distance: 200,
                                duration: 0.4,
                            },
                        },
                    },
                    particles: {
                        color: {
                            value: "#1E5128",
                        },
                        links: {
                            color: "#D8E9A8",
                            distance: 150,
                            enable: true,
                            opacity: 0,
                            width: 1,
                        },
                        collisions: {
                            enable: true,
                        },
                        move: {
                            directions: "none",
                            enable: true,
                            outModes: {
                                default: "bounce",
                            },
                            random: false,
                            speed: 3,
                            straight: false,
                        },
                        number: {
                            density: {
                                enable: true,
                                area: 800,
                            },
                            value: 80,
                        },
                        opacity: {
                            value: 0.3,
                        },
                        shape: {
                            type: "circle",
                        },
                        size: {
                            value: { min: 1, max: 5 },
                        },
                    },
                    detectRetina: true,
                }}
            
        />
        </div>
    )
}

export default Particle;