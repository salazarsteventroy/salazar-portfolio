import './index.scss'
import Loader from 'react-loaders'
import { useState, useEffect } from 'react';
import AnimatedLetters from "../AnimatedLetters";
// import TagCloud from "TagCloud";
import Sphere from '../Sphere';


const Skills = () => {
    const [letterClass, setLetterClass] = useState('text-animate');

    useEffect(() => {
        const timer = setTimeout(() => {
            setLetterClass('text-animate-hover');
        }, 3000);

        return () => {
            clearTimeout(timer);
        }
    });

    return(
        <>
            <div className="container skills-page">
                <div className="text-zone">
                    <h1>
                    <AnimatedLetters
                    letterClass={letterClass}
                    strArray={"Skills & Tools".split("")}
                    idx={15}
                        />
                    </h1>
                    <p>
                        A FullStack developer that is knowledgeable in technologies like

                    <span className="skills-span"> HTML5, CSS3, SASS JavaScript,MongoDB,Express, React, NodeJS, REST, Bootstrap, Postman and Git </span>

                         and platforms such as

                        <span className="skills-span"> Mongodb, AWS, Firebase, Heroku, Stripe and Vercel</span>
                    </p>
                    <br/>

                    <p>
                        Check out my CV with the button below :
                    </p>
                    
            
                    <a className = 'cv-button' href="https://drive.google.com/file/d/11rFa5hQkgGBXBN0A1LHWJth7GnHZq7CG/view?usp=sharing"  target="_blank" rel="noopener noreferrer">CV LINK HERE </a>
    
                </div>


                <div className="text-shpere">
                <Sphere />
                </div>

                
            </div>
            <Loader type="pacman" />
        </>
    )
}

export default Skills;
