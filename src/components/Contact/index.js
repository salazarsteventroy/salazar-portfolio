import Loader from 'react-loaders'
import './index.scss'
import AnimatedLetters from '../AnimatedLetters'
import { useState, useEffect, useRef } from 'react'
import emailjs from '@emailjs/browser'
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'


const Contact = () => {
    const [letterClass, setLetterClass] = useState('text-animate')
    const form = useRef()

    useEffect(() => {
        
        let timeoutId = setTimeout(() => {
            setLetterClass('text-animate-hover')
        }, 3000)
        
        return () => {
                    clearTimeout(timeoutId)
                }
    }, [])

    const sendEmail = (e) => {
        e.preventDefault()

        emailjs
        .sendForm('default_service', 'template_dv0rlff', form.current, 'M5n7wfSPb6K9v1Aam')
            .then(
                () => {
                    alert('message successfully sent')
                    window.location.reload(false)
                },
                () => {
                    alert('failed so send the message please try again')
                }
            )
    }
    return (
        <>
          <div className="container contact-page">
            <div className="text-zone">
              <h1>
                <AnimatedLetters
                  letterClass={letterClass}
                  strArray={['C', 'o', 'n', 't', 'a', 'c', 't', ' ', 'm', 'e']}
                  idx={15}
                />
              </h1>
              <p>
                I am interested in every opportunities I could grab - especially on ambitious projects. If you have any other requests or
                questions reach me by using the form below.
              </p>
              <div className="contact-form">
                <form ref={form} onSubmit={sendEmail}>
                  <ul>
                    <li className="half">
                      <input placeholder="Name" type="text" name="name" required />
                    </li>
                    <li className="half">
                      <input
                        placeholder="Email"
                        type="email"
                        name="email"
                        required
                      />
                    </li>
                    <li>
                      <input
                        placeholder="Subject"
                        type="text"
                        name="subject"
                        required
                      />
                    </li>
                    <li>
                      <textarea
                        placeholder="Message"
                        name="message"
                        required
                      ></textarea>
                    </li>
                    <li>
                      <input type="submit" className="flat-button" value="SEND" />
                    </li>
                  </ul>
                </form>
              </div>
            </div>
            <div className="info-map">
           Steven Troy Salazar,
              <br />
              Philippines
              <br />
              Angeles, Pampanga<br />
              <br />
              09973079369
              <br />
              <span>salazarsteventroy@gmail.com</span>
            </div>
            <div className="map-wrap">
              <MapContainer center={[14.4405209,120.9535987

]} zoom={13}>
                <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
                <Marker position={[14.4405209,120.9535987

]}>
                  <Popup>Message sent :)</Popup>
                </Marker>
              </MapContainer>
            </div>
          </div>
          <Loader type="pacman" />
        </>
      )
    }

export default Contact