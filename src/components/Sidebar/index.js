import './index.scss'
import { Link } from 'react-router-dom'
import LogoS from '../../assets/images/logo-s.png'
import LogoSubtitle from '../../assets/images/logo_sub.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faHome, faUser, faSuitcase, faBars, faClose, faTools } from '@fortawesome/free-solid-svg-icons'
import {
    faGitlab,
    faLinkedin
} from '@fortawesome/free-brands-svg-icons';

import { NavLink } from 'react-router-dom'
import { useState } from 'react'


const Sidebar = () => {
    const [showNav, setShowNav] = useState(false);
    return (
    <div className='nav-bar'>
        <Link className='logo' to ='/'>
            <img src={LogoS} alt="logo" />
            <img className="sub-logo"src={LogoSubtitle} alt="troy" />
        </Link>
        <nav className={showNav ? 'mobile-show' : ''}>
            <NavLink exact="true" 
            onClick={() => setShowNav(false)}
            activeclassname="active" to ="/">
                <FontAwesomeIcon icon={faHome} color="#c3dbc5" />
            </NavLink>
            <NavLink exact="true" 
            onClick={() => setShowNav(false)}
            activeclassname="active" className ="about-link" to ="/about">
                <FontAwesomeIcon icon={faUser} color="#c3dbc5" />
            </NavLink>
            <NavLink exact="true" 
            onClick={() => setShowNav(false)}
            activeclassname="active" className="skills-link" to ="/skills">
                <FontAwesomeIcon icon={faTools} color="#c3dbc5" />
            </NavLink>
            <NavLink exact="true"
            onClick={() => setShowNav(false)}
            activeclassname="active"  className="portfolio-link" to ="/portfolio">
                <FontAwesomeIcon icon={faSuitcase} color="#c3dbc5" />
            </NavLink>

            <NavLink exact="true" 
            onClick={() => setShowNav(false)}
            activeclassname="active" className="contact-link" to ="/contact">
                <FontAwesomeIcon icon={faEnvelope} color="#c3dbc5" />
            </NavLink>


            <FontAwesomeIcon 
          onClick={() => setShowNav(false)}
          icon={faClose}
          color="#ffd700"
          size="3x"
          className='close-icon' />
        </nav>

        <ul>
            <li>
                <a
                href="https://gitlab.com/salazarsteventroy"
                target="_blank"
                rel="noreferrer"
                >
                <FontAwesomeIcon
                    icon={faGitlab}
                    color="#4d4d4e"
                    className="anchor-icon"
                />

                </a>
                </li>
                <li>
          <a
            href="https://www.linkedin.com/in/steven-troy-salazar-8a6548226/"
            target="_blank"
            rel="noreferrer"
          >
            <FontAwesomeIcon
              icon={faLinkedin}
              color="#4d4d4e"
              className="anchor-icon"
            />
          </a>
        </li>

        </ul>
        <FontAwesomeIcon 
          onClick={() => setShowNav(true)}
          icon={faBars}
          color="#ffd700"
          size="3x"
          className='hamburger-icon' />
    </div>
    )
}

export default Sidebar;