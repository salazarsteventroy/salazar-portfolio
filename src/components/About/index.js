import { useEffect, useState } from 'react'
import {
  faCss3,
  faGitAlt,
  faHtml5,
  faJsSquare,
  faNodeJs,
  faReact,
} from '@fortawesome/free-brands-svg-icons'
import Loader from 'react-loaders'
import AnimatedLetters from '../AnimatedLetters'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './index.scss'
// import Particle from '../Particles'

const About = () => {
    const [letterClass, setLetterClass] = useState('text-animate')


    useEffect(() => {
        
        let timeoutId = setTimeout(() => {
            setLetterClass('text-animate-hover')
        }, 3000)
        
        return () => {
                    clearTimeout(timeoutId)
                }
    }, [])
 
    return(
        <> 
        <div className='container about-page'>
            <div className='text-zone'>
                <h1>
                    <AnimatedLetters
                    letterClass={letterClass}
                    strArray={['A','b','o','u','t',' ','M','e']}
                    idx={15}
                     />
                </h1>
                <p>
                I'm a Full Stack developer that is devoted to the idea of lifelong learning and has a great love for web development, My enthusiasm and passion for this field are driven by the special blend of creativity, logic, technology, and the never-ending search for new things to explore.
                </p>
                <p>
                As a Full Stack developer, I have worked hard to hone my skills, I am aware of the gaps that I have but I am willing to fill those gaps, learn new things and grow more and more and more ,a never ending "more" in this industry that is forever evolving.
                </p>
                <p>
                  I am aiming for a Junior Developer Position that would allow me to develop my skills while also making contributions through development for the company.
                </p>
            </div>
            <div className='particle-box'>
              {/* <Particle /> */}
            <div className="stage-cube-cont">
          <div className="cubespinner">
            <div className="face1">
              <FontAwesomeIcon icon={faNodeJs} color="#68A063" />
            </div>
            <div className="face2">
              <FontAwesomeIcon icon={faHtml5} color="#F06529" />
            </div>
            <div className="face3">
              <FontAwesomeIcon icon={faCss3} color="#28A4D9" />
            </div>
            <div className="face4">
              <FontAwesomeIcon icon={faReact} color="#5ED4F4" />
            </div>
            <div className="face5">
              <FontAwesomeIcon icon={faJsSquare} color="#EFD81D" />
            </div>
            <div className="face6">
              <FontAwesomeIcon icon={faGitAlt} color="#EC4D28" />
            </div>
                </div>
            </div>
        </div>
        </div>
        <Loader type="pacman" />
        </>
    )
    
}

export default About;